<?php

function verifyCPF( $cpf )
{
    $cpf = "$cpf";
    if (strpos($cpf, "-") !== false)
    {
        $cpf = str_replace("-", "", $cpf);
    }
    if (strpos($cpf, ".") !== false)
    {
        $cpf = str_replace(".", "", $cpf);
    }
    $sum = 0;
    $cpf = str_split( $cpf );
    $cpftrueverifier = array();
    $cpfnumbers = array_splice( $cpf , 0, 9 );
    $cpfdefault = array(10, 9, 8, 7, 6, 5, 4, 3, 2);
    for ( $i = 0; $i <= 8; $i++ )
    {
        $sum += $cpfnumbers[$i]*$cpfdefault[$i];
    }
    $sumresult = $sum % 11;
    if ( $sumresult < 2 )
    {
        $cpftrueverifier[0] = 0;
    }
    else
    {
        $cpftrueverifier[0] = 11-$sumresult;
    }
    $sum = 0;
    $cpfdefault = array(11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
    $cpfnumbers[9] = $cpftrueverifier[0];
    for ( $i = 0; $i <= 9; $i++ )
    {
        $sum += $cpfnumbers[$i]*$cpfdefault[$i];
    }
    $sumresult = $sum % 11;
    if ( $sumresult < 2 )
    {
        $cpftrueverifier[1] = 0;
    }
    else
    {
        $cpftrueverifier[1] = 11 - $sumresult;
    }
    $returner = false;
    if ( $cpf == $cpftrueverifier )
    {
        $returner = true;
    }


    $cpfver = array_merge($cpfnumbers, $cpf);

    if ( count(array_unique($cpfver)) == 1 || $cpfver == array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0) )

    {

        $returner = false;

    }
    return $returner;
}

function verifyDate($type, $date, $msg) {
    
	if($type == "number")
	{
		$date ? $var = preg_replace("/[\'\"\<\>]/", "", $date) : $var = $msg;
	} 
	else if ($type == "text")
	{
		$date ? $var = addslashes($date) : $var = $msg;
	}
	return $var;	
};

function changeDate($date){
	$date = explode("/", $date);
	$date = $date[2]."-".$date[1]."-".$date[0];
	return $date;
};

date_default_timezone_set('America/Sao_Paulo');

function dateDif($date1, $date2, $intervalo) {

    switch ($intervalo) {
        case 'y':
            $Q = 86400*365;
            break; //ano
        case 'm':
            $Q = 2592000;
            break; //mes
        case 'd':
            $Q = 86400;
            break; //dia
        case 'h':
            $Q = 3600;
            break; //hora
        case 'n':
            $Q = 60;
            break; //minuto
        default:
            $Q = 1;
            break; //segundo
    }

    return round((strtotime($date2) - strtotime($date1)) / $Q);
}

function sendData($url, $params, $type = 'post')
{
    $ch = curl_init();

    if($type == 'post'){
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    } else if($type == 'get'){
        curl_setopt($ch, CURLOPT_URL,$url.$params);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);
    // further processing ....
    if ($server_output['sucess'] == true) {
        return $server_output;
    } else {
        return 0;
    }
}

function aasort ($array, $key, $key1)
{
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    $array=$ret;
}

function searchFriends($list, $type, $em)
{
    $friends = $em->getRepository('Wingman\Entity\User')->findBy(array('id'.$type => $list));

    return $friends;
}

function getDataUser($user, $app, $em)
{

    if ($user->tokenFoursquare) {
        //cria novo obj usuario
        $app['foursquareAPI']->SetAccessToken($user->tokenFoursquare);
        //retorna os dados do usuario
        $response = $app['foursquareAPI']->GetPrivate("users/self");
        //retorna os amigos do usuario
        $resFriends = $app['foursquareAPI']->GetPrivate("users/self/friends");
        //json decode
        $data = json_decode($response);
        $dataUser['foursquare'] = $data->response->user;
        $dataFriends = json_decode($resFriends);
        //cria um array de ids de amigos
        foreach ($dataFriends->response->friends->items as $item) {
            $idFriends[] = $item->id;
        }
        //filtra os amigos em comum
        $friends['foursquare'] = searchFriends($idFriends, 'Foursquare', $em);
    }

    if ($user->tokenInstagram) {
        //cria novo obj usuario
        $app['instagramAPI']->setAccessToken($user->tokenInstagram);
        //retorna os dados do usuario
        $dataUser['instagram'] = $app['instagramAPI']->getUser();
        //retorna os amigos do usuario
        $dataFriends = $app['instagramAPI']->getUserFollows();
        //cria um array de ids de amigos
        foreach ($dataFriends->data as $item) {
            $idFriends[] = $item->id;
        }
        //filtra os amigos em comum
        $friends['instagram'] = searchFriends($idFriends, 'Instagram', $em);

    }

    if ($user->tokenTwitter) {
        //cria novo obj usuario
        $tokenTT = explode(",", $user->tokenTwitter);
        $_twitter = new TwitterOAuth(
            "Aqw2kOKaJcaqOcSCPQyvw",
            "G0QMUwMwU2O3EphqmH86qdhgdANLsDKnqgqkgdQj8",
            $tokenTT[0],
            $tokenTT[1]
        );
        //retorna os dados do usuario
        $dataUser['twitter'] = $_twitter->get('account/verify_credentials');
        //retorna um array de ids de amigos
        $friend = $_twitter->get('friends/ids');
        //filtra os amigos em comum
        $friends['twitter'] = searchFriends($friend->ids, 'Twitter', $em);

    }

    if ($user->tokenGooglePlus) {

        $googlePlus = null;
    }

    if ($user->idFacebook) {
        //dados do usuario logado
        $user1 = $app['user_detail'];
        //verifica se é o mesmo usuário logado esta solicitando informacoes da sua própria conta
        if($user1->username == $user->username){
            //usuário logado em sua própria página
            if ($app['facebookAPI']->getUser()) {
                $dataUser['facebook'] = $app['facebookAPI']->api('/me', 'GET');
                //retorna os amigos facebook
                $friend = $app['facebookAPI']->api('/me/friends');
                foreach ($friend['data'] as $item) {
                    $idFriends[] = $item['id'];
                }
                //filtra os amigos em comum (facebook/wingman)
                $friends['facebook'] = searchFriends($idFriends, 'Facebook', $em);
            } else {
                //a sessão facebook expirou

            }
        //usuário solicitando informação de um outro usuário
        } else {
            //retorna os dados de um usuário amigo ou não
            $dataUser['facebook'] = $app['facebookAPI']->api($user->idFacebook, 'GET');
            //tenta retornar os amigos deste usuário
            try{
                $query="SELECT uid, name, work_history FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 IN (SELECT uid FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = ".$user->idFacebook." ) and is_app_user=1) )";
                $friend = $app['facebookAPI']->api(array('method'=>'fql.query', 'query'=>$query));

                foreach ($friend['data'] as $item) {
                    $idFriends[] = $item['id'];
                }
                $friends['facebook'] = searchFriends($idFriends, 'Facebook', $em);
            } catch ( FacebookApiException $e ) {
                $friends['facebook'] = $e;
            }
        }

    }
    //junta os arrays com os dados do usuário e os amigos de cada rede social
    $result = array($dataUser, $friends);

    return $result;

}

function reorderLikesByPhoto($arrayPhotos){

    for ($i = 0, $a = 0; $i < count($arrayPhotos); $i++, $a++)
    {
        $arrPhoto[$a]['likes'] = $arrayPhotos[$i]->likes->count;
        $arrPhoto[$a]['image'] = $arrayPhotos[$i]->images->low_resolution->url;
    };

    arsort($arrPhoto);

    return $arrPhoto;

}