<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Silex\Application;

$callback = $app['controllers_factory'];

$callback->post('/log-in',function(Request $request) use ($app) {

    $msg=array();
    $name = $request->get('user_name') ? addslashes($request->get('user_name')) : array_push($msg, "NAME NOT FOUND");
    $pass = $request->get('user_pwd') ? addslashes($request->get('user_pwd')) : array_push($msg, "PASSWORD NOT FOUND");
    $email = $request->get('user_email') ? preg_replace("/[\'\"\<\>]/", "", $request->get('user_email')) : array_push($msg, "EMAIL NOT FOUND");
    $username = $request->get('user_username') ? addslashes($request->get('user_username')) : array_push($msg, "USERNAME NOT FOUND");
    $gender = $request->get('user_gender') ? addslashes($request->get('user_gender')) : null;

    $id = addslashes($request->get('id'));
    $token = addslashes($request->get('token'));
    $type = addslashes($request->get('type'));
    $picture = addslashes($request->get('picture'));

    if(count($msg) <= 1){
        $repo = $app['user_repository'];
        $user = $repo->createUser($name, $username, $pass, $email, $picture, $gender);
        if($id)
            $repo->insertCredentials($user, $id, $token, $type);

        if($user){
            return $app->redirect("/");
        } else {
            $msg = "Sorry something went wrong try again later.";
        }

    }

    return $app['twig']->render('page-login.twig', array(
        'title' => 'Wingman Login',
        'home'  => '../',
        'page_title' => 'Wingman Beer',
        'slogan' => 'slogan',
        'name' => 'Wingman',
        'type' => 'register',
        'description' => 'description',
        'name_button' => '',
        'message' => $msg,
        'error' => $app['security.last_error']($request),
        'button_botton' => false,
        'login4SQ' => $app['foursquareAPI']->AuthenticationLink()
    ));

})->bind('log-in');

$callback->get('/{page}', function(Silex\Application $occurrence, Request $request, $page) use ($app , $em) {

    $userProfile = [];
    $erroLink = null;
    $msg = null;
    if($page == 'foursquare'){
        $code = $app['request']->get('code');

        if($code){
            //We need to hit up the authkey URL and get the key in JSON format
            $token = $app['foursquareAPI']->GetToken($code, $app['myUrl']."/callback/foursquare");
            if(!isset($token)){
                $erroLink = $app['foursquareAPI']->AuthenticationLink();
            }else{
                $app['session']->set('tokenFoursquare', $token);
                $app['foursquareAPI']->SetAccessToken($token);
                $response = $app['foursquareAPI']->GetPrivate("users/self");
                $data = json_decode($response);
                $users = $data->response->user;
                if($users){

                    $userProfile['firstName'] = $users->firstName.' '.$users->lastName;
                    $userProfile['id'] = $users->id;
                    $userProfile['gender'] = $users->gender;
                    $userProfile['email'] = $users->contact->email;
                    $userProfile['picture'] = $users->photo->prefix."120x120".$users->photo->suffix;
                    $userProfile['userName'] = strtolower($users->firstName.$users->lastName);

                    $userProfile['token'] = $token;
                    $userProfile['type'] = "Foursquare";

                    if(!$users->id)
                        $erroLink = $app['foursquareAPI']->AuthenticationLink();

                } else {
                    $erroLink = $app['foursquareAPI']->AuthenticationLink();
                }
            }

        }
    } else if ($page == 'instagram'){

        $code = $request->get('code');

        if (isset($code)) {
            // receive OAuth token object
            $data = $app['instagramAPI']->getOAuthToken($code);

            if($data->code == 200){
                $username = $data->user;

                // store user access token
                $app['instagramAPI']->setAccessToken($data->access_token);
                $app['session']->set('tokenInstagram', $data->access_token);
                // now you have access to all authenticated user methods
                $users = $app['instagramAPI']->getUserMedia();

                $userProfile['userName'] = $username->username;
                $userProfile['firstName'] = $username->full_name;
                $userProfile['id'] = $username->id;
                $userProfile['picture'] = $username->profile_picture;
                $userProfile['token'] = $data->access_token;
                $userProfile['type'] = "Instagram";

                if(!$username->id)
                    $erroLink = $app['instagramAPI']->getLoginUrl();

            } else {
                $erroLink = $app['instagramAPI']->getLoginUrl();
            }



        } else {
            // check whether an error occurred
            if (isset($_GET['error'])) {
                $erroLink = $app['instagramAPI']->getLoginUrl();
            }
        }

    } else if ($page == 'facebook'){

        if($app['facebookAPI']->getUser()){
            $user = $app['facebookAPI']->api('/me','GET');
            $app['session']->set('tokenFacebook', $app['facebookAPI']->getAccessToken());
            $userProfile['firstName'] = $user['name'];
            $userProfile['id'] = $user['id'];
            $userProfile['gender'] = $user['gender'];
            $userProfile['email'] = $user['email'];
            $userProfile['picture'] = "http://graph.facebook.com/".$user['username']."/picture";
            $userProfile['userName'] = $user['username'];
            $userProfile['token'] = $app['facebookAPI']->getAccessToken();
            $userProfile['type'] = "Facebook";
            if(!$user['id'])
                $erroLink = $app['facebookAPI']->getLoginUrl($app['facebookParams']);

        } else {
            $erroLink = $app['facebookAPI']->getLoginUrl($app['facebookParams']);
        }


    } else if ($page == 'twitter'){

        $_twitter = new TwitterOAuth(
            "Aqw2kOKaJcaqOcSCPQyvw",
            "G0QMUwMwU2O3EphqmH86qdhgdANLsDKnqgqkgdQj8",
            // use access token from previous call /twitter/
            $request->get('oauth_token'),
            $app['session']->get('twitterTokenSecret')
        );

        try
        {
            $access_token = $_twitter->getAccessToken($request->get('oauth_verifier'));
            $app['session']->set('tokenTwitter', $access_token);

            $user = $_twitter->get('account/verify_credentials');
            $userProfile['firstName'] = $user->name;
            $userProfile['id'] = $user->id;
            $userProfile['userName'] = $user->screen_name;
            $userProfile['picture'] = $user->profile_image_url;
            $userProfile['token'] = $access_token['oauth_token'].",".$access_token['oauth_token_secret'];
            $userProfile['type'] = "Twitter";

            if(!$user->id)
                $erroLink = $app['twitterAPI']->getAuthorizeURL($app['session']->get('twitterTT'));

        }
        catch(OAuthException $ex)
        {
            $erroLink = $app['twitterAPI']->getAuthorizeURL($app['session']->get('twitterTT'));
        }

    }

    if ($app['security']->isGranted('ROLE_USER')) {

        $userL = $app['security']->getToken()->getUser();
        //$em = $this->getDoctrine()->getManager();
        $userById = $em->getRepository('Wingman\Entity\User')->find($userL->id);

        if (!$userById) {
            throw $this->createNotFoundException(
                'User not found: '.$userL->username
            );
        }



        switch ($userProfile['type'])
        {
            case 'Foursquare':
                $userById->setIdFoursquare($userProfile['id']);
                $userById->setTokenFoursquare($userProfile['token']);
                break;
            case 'Instagram':
                $userById->setIdInstagram($userProfile['id']);
                $userById->setTokenInstagram($userProfile['token']);
                break;
            case 'Twitter':
                $userById->setIdTwitter($userProfile['id']);
                $userById->setTokenTwitter($userProfile['token']);
                break;
            case 'Google':
                $userById->setIdGooglePlus($userProfile['id']);
                $userById->setTokenGooglePlus($userProfile['token']);
                break;
            case 'Facebook':
                $userById->setIdFacebook($userProfile['id']);
                $userById->setTokenFacebook($userProfile['token']);
                break;
        }

        $verifyId = $em->getRepository('Wingman\Entity\User')->findBy(array('id'.$userProfile['type'] => $userProfile['id']));

        if(($verifyId[0]->username) && ($verifyId[0]->username != $userL->username)){
            $app['session']->set('error', "Sorry, could not register that ".$userProfile['type']." id in your account, it is already being used by another user");
            return $app->redirect("/profile");
        }


        try
        {
            $em->flush();
            return $app->redirect("/profile");
        }
        catch(OAuthException $ex)
        {
            throw $this->createNotFoundException(
                'Error database: '.$ex
            );
        }


    }

    return $app['twig']->render('page-login.twig', array(
        'userProfile' => $userProfile,
        'title' => 'Wingman Login',
        'home'  => '../',
        'page_title' => 'Wingman Beer',
        'slogan' => 'slogan',
        'name' => 'Wingman',
        'type' => 'register',
        'description' => 'description',
        'message' => $msg,
        'erroLink' => $erroLink,
        'messageHtml'   => "We're almost ready to show you everything we have, just complete the fields beside and we can start.",
        'name_button' => '',
        'button_botton' => false
    ));

});

return $callback;