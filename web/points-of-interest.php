<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Silex\Application;

$poi = $app['controllers_factory'];

$poi->get('/points-of-interest',function() use ($app) {
    return $app['twig']->render('attraction.twig');
})->bind('/points-of-interest');

return $poi;