<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Silex\Application;

$login = $app['controllers_factory'];

$login->get('/login', function (Request $request) use ($app) {

    if ($app['security']->isGranted('ROLE_USER')) {
        return $app->redirect("profile");
    }

    return $app['twig']->render('page-login.twig', array(
        'error' => $app['security.last_error']($request),
        'last_username' => $app['session']->get('_security.last_username'),
        'title' => 'Wingman Login',
        'home' => '../',
        'page_title' => 'Wingman Beer',
        'slogan' => 'slogan',
        'name' => 'Wingman',
        'description' => 'description',
        'name_button' => '',
        'button_botton' => false,
        'type' => 'login',
        'userProfile' => "",
        'login4SQ' => $app['foursquareAPI']->AuthenticationLink(),
        'loginInsta' => $app['instagramAPI']->getLoginUrl(),
        'loginFace' => $app['facebookAPI']->getLoginUrl($app['facebookParams']),
        'loginTwitter' => $app['twitterAPI']->getAuthorizeURL($app['session']->get('twitterTT')),
        'loginGoogle' => $app['googleAPI']->createAuthUrl()
    ));

})->bind('login');

$login->get('/profile', function (Request $request) use ($app, $em) {

    if (!$app['security']->isGranted('ROLE_USER')) {
        return $app->redirect("/login");
    }

    $user = $app['user_detail'];
    $result = getDataUser($user, $app, $em);

    $error = $app['session']->get('error');
    $app['session']->remove('error');

    $dataFriendsArray = array_unique($result[1]);

    return $app['twig']->render('helpers/user-detail.twig', array(
        'user' => $user,
        'dataUser' => $result[0],
        'friends' => $dataFriendsArray,
        'title' => 'Wingman Profile',
        'home' => '../',
        'page_title' => 'Wingman Beer',
        'slogan' => 'slogan',
        'name' => 'Wingman',
        'description' => 'description',
        'message' => $error,
        'name_button' => '',
        'button_botton' => false,
        'login4SQ' => $app['foursquareAPI']->AuthenticationLink(),
        'loginInsta' => $app['instagramAPI']->getLoginUrl(),
        'loginFace' => $app['facebookAPI']->getLoginUrl($app['facebookParams']),
        'loginTwitter' => $app['twitterAPI']->getAuthorizeURL($app['session']->get('twitterTT')),
        'loginGoogle' => $app['googleAPI']->createAuthUrl()
    ));

})->bind('profile');

$login->get('/profile/{username}', function (Request $request) use ($app, $em) {


    if (!$app['security']->isGranted('ROLE_USER')) {
        return $app->redirect("/login");
    }

    $username = $request->get('username');
    $repo = $em->getRepository('Wingman\Entity\User');
    $user = $repo->loadUserByUsername($username);

    $result = getDataUser($user, $app, $em);

    $error = $app['session']->get('error');
    $app['session']->remove('error');

    $dataFriendsArray = array_unique($result[1]);

    return $app['twig']->render('helpers/user-detail.twig', array(
        'user' => $user,
        'dataUser' => $result[0],
        'friends' => $dataFriendsArray,
        'title' => 'Wingman Profile',
        'home' => '../',
        'page_title' => 'Wingman Beer',
        'slogan' => 'slogan',
        'name' => 'Wingman',
        'description' => 'description',
        'message' => $error,
        'name_button' => '',
        'visitor' => true,
        'button_botton' => false,
        'login4SQ' => $app['foursquareAPI']->AuthenticationLink(),
        'loginInsta' => $app['instagramAPI']->getLoginUrl(),
        'loginFace' => $app['facebookAPI']->getLoginUrl($app['facebookParams']),
        'loginTwitter' => $app['twitterAPI']->getAuthorizeURL($app['session']->get('twitterTT')),
        'loginGoogle' => $app['googleAPI']->createAuthUrl()
    ));

});

return $login;