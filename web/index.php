<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);
//xdebug_get_code_coverage();

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Silex\Application;

require_once __DIR__.'/../vendor/cosenary/instagram/instagram.class.php';
require_once __DIR__.'/../vendor/foursquare/foursquare.class.php';
require_once __DIR__.'/../vendor/facebook/php-sdk/src/facebook.php';
require_once __DIR__.'/../vendor/google/google-api-php-client/src/Google_Client.php';
require_once __DIR__.'/../vendor/google/google-api-php-client/src/contrib/Google_PlusService.php';
require_once __DIR__.'/../vendor/twitteroauth/twitteroauth.php';
require_once __DIR__.'/../vendor/j7mbo/twitter-api-php/TwitterAPIExchange.php';
require_once 'bootstrap.php';
require_once 'functions.php';

$app->mount("/", include 'login.php');
$app->mount("/map", include 'mapHome.php');
$app->mount("/callback", include 'callback.php');
$app->mount("/pub", include 'pub.php');
$app->mount("/points-of-interest", include 'points-of-interest.php');

$login->get('/', function () use ($app) {
    return $app['twig']->render('map.twig', array(
        'pubs' => null,
        'poi' => null,
        'user' => $app['session']->get('user'),
        'title' => 'Wingman Home',
        'home' => '../',
        'page_title' => 'Wingman Beer',
        'slogan' => 'slogan',
        'name' => 'Wingman',
        'description' => 'description',
        'name_button' => 'Login',
        'lat' => null,
        'lng' => null,
        'map' => true,
        'button_botton' => true
    ));
});

$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Headers', 'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, Origin');
    $response->headers->set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
});

$app->run();