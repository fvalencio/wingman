<?php

use Doctrine\ORM\Tools\Setup,
    Doctrine\ORM\EntityManager,
    Doctrine\Common\EventManager as EventManager,
    Doctrine\ORM\Events,
    Doctrine\ORM\Configuration,
    Doctrine\Common\Cache\ArrayCache as Cache,
    Doctrine\Common\Annotations\AnnotationRegistry,
    Doctrine\Common\Annotations\AnnotationReader,
    Doctrine\Common\ClassLoader;

use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Symfony\Component\HttpFoundation\Session\Session;
$cache = new Doctrine\Common\Cache\ArrayCache;

$annotationReader = new Doctrine\Common\Annotations\AnnotationReader;
$cachedAnnotationReader = new Doctrine\Common\Annotations\CachedReader(
    $annotationReader, // use reader
    $cache // and a cache driver
);

$driverChain = new Doctrine\ORM\Mapping\Driver\DriverChain();
// load superclass metadata mapping only, into driver chain
// also registers Gedmo annotations.NOTE: you can personalize it
Gedmo\DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
    $driverChain, // our metadata driver chain, to hook into
    $cachedAnnotationReader // our cached annotation reader
);

// now we want to register our application entities,
// for that we need another metadata driver used for Entity namespace
$annotationDriver = new Doctrine\ORM\Mapping\Driver\AnnotationDriver(
    $cachedAnnotationReader, // our cached annotation reader
    array(__DIR__ . DIRECTORY_SEPARATOR . '../src')
);
// NOTE: driver for application Entity can be different, Yaml, Xml or whatever
// register annotation driver for our application Entity namespace
$driverChain->addDriver($annotationDriver, 'Wingman');

$config = new Doctrine\ORM\Configuration;
$config->setProxyDir('/tmp');
$config->setProxyNamespace('Proxy');
$config->setAutoGenerateProxyClasses(true); // this can be based on production config.
// register metadata driver
$config->setMetadataDriverImpl($driverChain);
// use our allready initialized cache driver
$config->setMetadataCacheImpl($cache);
$config->setQueryCacheImpl($cache);

AnnotationRegistry::registerFile(__DIR__. DIRECTORY_SEPARATOR . '../vendor' . DIRECTORY_SEPARATOR . 'doctrine' . DIRECTORY_SEPARATOR . 'orm' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'Doctrine' . DIRECTORY_SEPARATOR . 'ORM' . DIRECTORY_SEPARATOR . 'Mapping' . DIRECTORY_SEPARATOR . 'Driver' . DIRECTORY_SEPARATOR . 'DoctrineAnnotations.php');

// Third, create event manager and hook prefered extension listeners
$evm = new Doctrine\Common\EventManager();
// gedmo extension listeners

// sluggable
$sluggableListener = new Gedmo\Sluggable\SluggableListener;
// you should set the used annotation reader to listener, to avoid creating new one for mapping drivers
$sluggableListener->setAnnotationReader($cachedAnnotationReader);
$evm->addEventSubscriber($sluggableListener);
//getting the EntityManager
$em = EntityManager::create(
    array(
        'driver'  => 'pdo_mysql',
        'host'    => '0.0.0.0',
        'port'    => '3306',
        'user'    => 'root',
        'password'  => 'root',
        'dbname'  => 'wingman',
    ),
    $config,
    $evm
);
//start application
$app = new Silex\Application();
$app['debug'] = true;
//provider session
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
//start session
$app['session'] = $app->share(function ($app) {
    return new Session();
});
$app['session']->start();
//share app
$app['user_repository'] = $app->share(function($app) use ($em) {
    $user = new Wingman\Entity\User;
    $repo = $em->getRepository('Wingman\Entity\User');
    $repo->setPasswordEncoder($app['security.encoder_factory']->getEncoder($user));
    $app['session']->set('user', $repo->objectToArray($user));
    return $repo;

});

$app['user_detail'] = $app->share(function($app) use ($em) {
    $user = $app['security']->getToken()->getUser();
    $repo = $em->getRepository('Wingman\Entity\User');
    $userObj = $repo->loadUserByUsername($user->username);

    return $userObj;
});

$app['myUrl'] = $app->share(function ($app) {
    return "http://wingman.dev:8888";
});
//register twig path
$app->register(new Silex\Provider\TwigServiceProvider(),array(
    'twig.path' => __DIR__ .'/views',
));
//register security to login
$app->register(new SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'anonymous' => true,
            'pattern' => '^/',
            'form' => array('login_path' => '/login', 'check_path'=>'/admin/login_check'),
            'users' => $app->share(function() use ($app) {
                    return $app['user_repository'];
                }),
            'logout' => array('logout_path'=>'/admin/logout')
        )
    )
));
//roles for firewall
$app['security.access_rules'] = array(
    array('^/admin','ROLE_ADMIN')
);
//share app API social
$app['instagramAPI'] = $app->share(function ($app) {
    return new Instagram(array(
        'apiKey'      => '14b66f93a35d450ca4d5b4d39e14a9c2',
        'apiSecret'   => 'bcc6a15570424ce5b095ddfe0ab21c6d',
        'apiCallback' => $app['myUrl'].'/callback/instagram'
    ));
});

$app['foursquareAPI'] = $app->share(function ($app) {
    return new Foursquare(
        "3F2VZFBALX1C3GSPN3OUVPYMVHAJPQPC4KA2KFT4CT2PEC2F",
        "JWQU5KC5TINEKRIZIZX4EGQUKUFKSTEW5HVX1JYIUUC32YSH",
        $app['myUrl']."/callback/foursquare"
    );
});

$app['facebookAPI'] = $app->share(function ($app) {
    return new Facebook(array(
        'appId' => '624446070962311',
        'secret' => '09823891a6a5f25038c354c2fdcb07aa',
        'cookie' => true,
        'fileUpload' => false, // optional
        'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
    ));
});

$app['facebookParams'] = $app->share(function ($app) {
    return array(
        'scope' => 'basic_info, email, user_checkins, friends_checkins, user_location, read_friendlists, read_stream, publish_actions, friends_likes',
        'redirect_uri' => $app['myUrl'].'/callback/facebook'
    );
});

$app['googleAPI'] = $app->share(function ($app) {
    $client = new Google_Client();
    $client->setApplicationName("Wingman");
    $client->setClientId("890641058653.apps.googleusercontent.com");
    $client->setClientSecret("qt6GqqdWQyAbzSLYDmdc5a8i");
    $client->setRedirectUri($app['myUrl'].'/callback/googleplus');
    $client->setScopes(array('scope' => 'https://www.googleapis.com/auth/plus.login'));
    $client->setAccessType('online'); // default: offline
    $client->setDeveloperKey('AIzaSyAwt5TfNJeuvSYOMa6I7rugGsw4D8ysTtU'); // API key

    return $client;
    return new Google_PlusService($client);
});

$app['twitterAPI'] = $app->share(function ($app) {

    $_twitter = new TwitterOAuth("Aqw2kOKaJcaqOcSCPQyvw", "G0QMUwMwU2O3EphqmH86qdhgdANLsDKnqgqkgdQj8");
    $arrtoken = $_twitter->getRequestToken('http://'.$app['request']->headers->get('host').'/callback/twitter');
    $app['session']->set('twitterTT', $arrtoken);
    $app['session']->set('twitterToken', $arrtoken['oauth_token']);
    $app['session']->set('twitterTokenSecret', $arrtoken['oauth_token_secret']);

    return $_twitter;
});

return $app;