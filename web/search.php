<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Jcroll\FoursquareApiClient\Client\FoursquareClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Silex\Application;

$search = $app['controllers_factory'];
//defining our necessary keys and url here
$oauth = array(
    'id'		=> 'U0X013XFKEPZONZABFVTGUIX3SLXFMFHGWT3BQI4NV25ZJGW', //ENTER YOUR CLIENT ID HERE
    'secret'    => '3NLCBGUGK3TMTPWGSMWAHVMOBACK3UW2FE2DMK24XASUTFL2', //ENTER YOUR SECRET ID HERE
    'redirect' 	=> 'http://fabiovalencio.com/wingman/web/index.php/callback' //ENTER YOUR REDIRECT URI HERE
);

// search pubs through lat and lgn and foursquare oauth
$search->post('/pubs', function (Silex\Application $occurrence, Request $request) use ($app, $oauth) {
		// start foursquare	
		$fsq = FoursquareClient::factory(array(
		    'client_id'     => $oauth['id'],    // required
		    'client_secret' => $oauth['secret'] // required
		));
		// optionaly pass in for user specific requests
		$fsq->addToken($request->get('access_token')); 
		// requests with latitude and longitude
		if($request->get('ll')){
			$command = $fsq->getCommand('venues/search', array(
			    'll' => $request->get('ll'),
			    'query' => 'pub'
			));
		// requests without latitude or longitude
		} else {
			$command = $fsq->getCommand('venues/search', array(
			    'near' => 'London, Greater London',
			    'query' => 'pub'
			));
		}
		// returns an array of results		
		$pubs = $command->execute(); 
		$pubs = $pubs['response']['venues'];
		// Obtain a list of columns
		foreach ($pubs as $key => $row) {
		    $mid[$key]  = $row['stats']['checkinsCount'];
		}		
		// Sort the data with mid descending
		// Add $data as the last parameter, to sort by the common key
		array_multisort($mid, SORT_DESC, $pubs);
		//return results
		return $app->json($pubs, 200);
		
})->before(function (Request $request) {
	verifyData("number", $request->get('ll'), "ERROR: LL NOT FOUND");
	verifyData("text", $request->get('access_token'), "ERROR: ACCESS TOKEN NOT FOUND");
	$result = preg_grep("/ERROR.*/", $settings);
	if(count($result) > 0){
		return $app->json(array("Error"=> true, "Message" => $result), 404);
	}
});

$app->get('/googlell', function() use($app){

    $sql = "SELECT * from pubs WHERE lat = ''";
    $result = $app['mysql']->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    foreach($result as $row) {
        $address = $row['address'];
        $id = $row['id'];

        $response = sendData("https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false&key=AIzaSyABg3_lAn7VYyoxp8SaM98ZarA7S-2LQkQ", null);
        $response_a = json_decode($response);

        $arrQuery =
            array(
                "lat" => $response_a->results[0]->geometry->location->lat,
                "lng" => $response_a->results[0]->geometry->location->lng
            );

        if($arrQuery['lat']){
            $sql = atualizar('pubs', $arrQuery, "id = ".$id);
            $req = $app['mysql']->prepare($sql);
            $req->execute();
        }
    }

    $sql = "SELECT * from pubs ";
    $results = $app['mysql']->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    return $app->json($results, 200);

})->bind('googlell');

$app->get('/foursquarell', function() use($app, $oauth){
    $sql = "SELECT * from pubs WHERE IdFoursquare = ''";
    $result = $app['mysql']->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    foreach($result as $row) {

        $id = $row['id'];
        $end = $row['address'];
        $name = $row['name'];
        $ll = $row['lat'].",".$row['lng'];
        try{
            //$response = sendData("https://api.foursquare.com/v2/venues/explore", "?client_id=".$oauth['id']."&client_secret=".$oauth['secret']."&query=".$name."&ll=".$ll, "get");
            $response = sendData("https://api.foursquare.com/v2/venues/explore", "?client_id=".$oauth['id']."&client_secret=".$oauth['secret']."&categoryId=4bf58dd8d48988d11b941735&v=20140228&ll=".$ll, "get");
        } catch(Exception $e) {
            continue;
        };

        $pubs_a = json_decode($response);
        $pubs = $pubs_a->response->groups[0]->items;

        for($x=0; $x<count($pubs); $x++){

            $venue = get_object_vars($pubs[$x]);
            $pub = $venue['venue'];
            $pub = get_object_vars($pub);
            $location = get_object_vars($pub['location']);

            $arrayPub =
                array(
                    "nameFoursquare"=> $pub["name"], //strpos($pub["name"], "'") ? mysql_real_escape_string($pub["name"]) : $pub["name"],
                    "idFoursquare"  => $pub['id'],
                    "address"       => $location["address"], //strpos($location["address"], "'") ? mysql_real_escape_string($location["address"]) : $location["address"],
                    "lat"           => $location['lat'],
                    "lng"           => $location['lng']
                );

            similar_text($end, $location['address'], $perct);
            if ($perct > 50) {
                similar_text($name, $pub['name'], $percent);
                if($percent > 55){
                    try{
                        $sql = atualizar('pubs', $arrayPub, "id = ".$id);
                        $req = $app['mysql']->prepare($sql);
                        $req->execute();
                    } catch(Exception $e) {
                        echo $sql;
                        echo '</br>';
                        echo $e;
                        echo '</br>';
                        continue;
                    };

                }
            }
        }

    }

    try{
        $sql = "SELECT * from pubs WHERE IdFoursquare != ''";
        $results = $app['mysql']->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        return $app->json($results, 200);
    } catch(Exception $e) {
        return $app->json($e, 202);
    }

})->bind('foursquarell');

$app->get('/instagramll', function() use($app, $oauth){
    $sql = "SELECT * from pubs WHERE idInstagram = ''";
    $result = $app['mysql']->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    foreach($result as $row) {

        $id = $row['id'];
        $end = $row['address'];
        $name = $row['name'];
        $ll = $row['lat'].",".$row['lng'];
        try{
            $response = sendData("https://api.instagram.com/v1/locations/search", "?access_token=1855841.14b66f9.bf53263717174f1eb53171939a7562a6&lat=".$row['lat']."&lng=".$row['lng'], "get");
        } catch(Exception $e) {
            continue;
        };
        echo "https://api.instagram.com/v1/locations/search?access_token=1855841.14b66f9.bf53263717174f1eb53171939a7562a6&lat=".$row['lat']."&lng=".$row['lng'];
        $pubs_a = json_decode($response);
        print_r($pubs_a);
        die();
        $pubs = $pubs_a->response->groups[0]->items;

        for($x=0; $x<count($pubs); $x++){

            $venue = get_object_vars($pubs[$x]);
            $pub = $venue['venue'];
            $pub = get_object_vars($pub);
            $location = get_object_vars($pub['location']);

            $arrayPub =
                array(
                    "nameFoursquare"=> $pub["name"], //strpos($pub["name"], "'") ? mysql_real_escape_string($pub["name"]) : $pub["name"],
                    "idFoursquare"  => $pub['id'],
                    "address"       => $location["address"], //strpos($location["address"], "'") ? mysql_real_escape_string($location["address"]) : $location["address"],
                    "lat"           => $location['lat'],
                    "lng"           => $location['lng']
                );

            similar_text($end, $location['address'], $perct);
            if ($perct > 50) {
                similar_text($name, $pub['name'], $percent);
                if($percent > 55){
                    try{
                        $sql = atualizar('pubs', $arrayPub, "id = ".$id);
                        $req = $app['mysql']->prepare($sql);
                        $req->execute();
                    } catch(Exception $e) {
                        echo $sql;
                        echo '</br>';
                        echo $e;
                        echo '</br>';
                        continue;
                    };

                }
            }
        }

    }

    try{
        $sql = "SELECT * from pubs WHERE IdFoursquare != ''";
        $results = $app['mysql']->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        return $app->json($results, 200);
    } catch(Exception $e) {
        return $app->json($e, 202);
    }

})->bind('instagramll');

$app->get('/instagramll', function() use($app){
    $sql = "SELECT * from pubs WHERE idInstagram = '' AND idFoursquare = '' ";
    $result = $app['mysql']->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    $app['instagramAPI']->setAccessToken($app['session']->get('instagramAccessToken'));
    //return $app->json($instagramAPI->searchLocation(40.758896,-73.985131), 200);
    foreach($result as $row) {

        $id = $row['id'];
        $name = $row['name'];
        $id4SQ = $row['idFoursquare'];

        $pubs = $app['instagramAPI']->searchLocation(floatval($row['lat']), floatval($row['lng']));
        $pub = $pubs['data'];
        if($pubs == null)
            continue;


        $arrayPub =
            array(
                "nameInstagram" => $pub["name"], //strpos($pub["name"], "'") ? mysql_real_escape_string($pub["name"]) : $pub["name"],
                "idInstagram"   => $pub['id'],
                "lat"           => $pub['latitude'],
                "lng"           => $pub['longitude']
            );

        $arrayPub2 =
            array(
                "nameInstagram" => $pub["name"], //strpos($pub["name"], "'") ? mysql_real_escape_string($pub["name"]) : $pub["name"],
                "idInstagram"   => $pub['id']
            );

        similar_text($name, $pub['name'], $percent);

        if($percent > 55){
            try{
                if($id4SQ > 0){
                    $sql = atualizar('pubs', $arrayPub2, "id = ".$id);
                } else {
                    $sql = atualizar('pubs', $arrayPub, "id = ".$id);
                }
                $sql = atualizar('pubs', $arrayPub, "id = ".$id);
                $req = $app['mysql']->prepare($sql);
                $req->execute();
            } catch(Exception $e) {
                echo $sql;
                echo '</br>';
                echo $e;
                echo '</br>';
                continue;
            };

        }
    }

    try{
        $sql = "SELECT * from pubs WHERE idInstagram != ''";
        $results = $app['mysql']->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        return $app->json($results, 200);
    } catch(Exception $e) {
        return $app->json($e, 202);
    }

})->bind('instagramll');


$search->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
});


return $search;
