<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Silex\Application;

$map = $app['controllers_factory'];

$map->get('/{ll}', function (Silex\Application $occurrence, Request $request) use ($app) {

    $possition = explode(",", $request->get('ll'));
    $lat = $possition[0];
    $lng = $possition[1];
    //Pubs
    $endpoint = "venues/search";
    $params = array(
        'll' => $request->get('ll'),
        'categoryId' => '4bf58dd8d48988d11b941735',
        'radius' => '1000'
    );
    $response = $app['foursquareAPI']->GetPublic($endpoint, $params);
    $venues = json_decode($response);
    $pubs = $venues->response->venues;
    //Obtain a list of columns
    foreach ($pubs as $key => $row) {
        $mid[$key] = $row->stats->checkinsCount;
    }
    //Sort the data with mid descending
    //Add $data as the last parameter, to sort by the common key
    array_multisort($mid, SORT_DESC, $pubs);
    //Points of interest
    $params = array(
        'll' => $request->get('ll'),
        'categoryId' => '4bf58dd8d48988d12d941735',
        'radius' => '10000'
    );
    $response = $app['foursquareAPI']->GetPublic($endpoint, $params);
    $venues = json_decode($response);
    $poi = $venues->response->venues;
    // Obtain a list of columns
    foreach ($poi as $key => $row) {
        $mid[$key] = $row->stats->checkinsCount;
    }
    // Sort the data with mid descending
    // Add $data as the last parameter, to sort by the common key
    array_multisort($mid, SORT_DESC, $poi);

    //return imagems from instagram for each pub
    foreach ($pubs as $key => $row) {
        $venueInsta = $app['instagramAPI']->searchLocation(array("foursquare_v2_id" => $row->id, "distance" => "100"));
        foreach ($venueInsta->data as $k => $v) {
            similar_text($v->name, $row->name, $perct);
            if ($perct > 70) {
                $photosInsta = $app['instagramAPI']->getLocationMedia($v->id);
            }
        }
        $pubs[$key]->instagram = $photosInsta->data;
    }

    return $app['twig']->render('map.twig', array(
        'pubs' => $pubs,
        'poi' => $poi,
        'user' => $app['session']->get('user'),
        'title' => 'Wingman Home',
        'home' => '../',
        'page_title' => 'Wingman Beer',
        'slogan' => 'slogan',
        'name' => 'Wingman',
        'description' => 'description',
        'name_button' => 'Login',
        'lat' => $lat,
        'lng' => $lng,
        'map' => true,
        'button_botton' => true
    ));

})->bind('map');

return $map;