<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Silex\Application;

$pub = $app['controllers_factory'];

$app->get('/venue/{id}',function(Silex\Application $occurrence, Request $request, $id) use ($app) {
    if($app['session']->get('user')){

        $user = $app['user_detail'];

        if($user->tokenFoursquare){
            $app['foursquareAPI']->SetAccessToken($user->tokenFoursquare);
        } else {
            #TODO
            #contar requisicoes por usuario
        }

        //Venues
        $endpoint = "venues/".$id;
        $response = $app['foursquareAPI']->GetPublic($endpoint);
        $venues = json_decode($response);
        $pub = $venues->response->venue;

        //Photos
        $endpoint = "venues/".$id."/photos";
        $response = $app['foursquareAPI']->GetPublic($endpoint);
        $venues = json_decode($response);
        $photo = $venues->response->photos->items;

        return $app['twig']->render('pub.twig', array(
            'user' => $app['session']->get('user'),
            'pub' => $pub,
            'photos' => $photo,
            'title' => 'Wingman',
            'home'  => '../',
            'page_title' => 'Wingman Beer',
            'slogan' => 'slogan',
            'name' => 'Wingman',
            'description' => 'description',
            'name_button' => 'Login',
            'button_botton' => true
        ));

    } else {
        return $app->redirect($app['url_generator']->generate('/login'));
    }

})->bind('/venue');

return $pub;