<?php

namespace Wingman\Entity;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserProviderInterface
{
    private $passwordEncoder;

    public function createUser($name, $username, $password, $email, $picture, $gender)
    {
        $user = new User();
        $user->name = $name;
        $user->username = $username;
        $user->plainPassword = $password;
        $user->email = $email;
        $user->picture = $picture;
        $user->gender = $gender;
        $user->roles = "ROLE_USER";
        $this->insert($user);

        return $user;
    }

    public function setPasswordEncoder(PasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function insert($user)
    {
        $this->encodePassword($user);
        $user->plainPassword = "";
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function insertCredentials($user, $id, $token, $type)
    {
        switch ($type)
        {
            case 'Foursquare':
                $user->setIdFoursquare($id);
                $user->setTokenFoursquare($token);
                break;
            case 'Instagram':
                $user->setIdInstagram($id);
                $user->setTokenInstagram($token);
                break;
            case 'Twitter':
                $user->setIdTwitter($id);
                $user->setTokenTwitter($token);
                break;
            case 'Google':
                $user->setIdGooglePlus($id);
                $user->setTokenGooglePlus($token);
                break;
            case 'Facebook':
                $user->setIdFacebook($id);
                $user->setTokenFacebook($token);
                break;
        }

        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function objectToArray(User $user)
    {
        return array(
            'id' => $user->id,
            'username' => $user->username,
            'name' => $user->name,
            'password' => $user->password,
            'email' => $user->email,
            'picture' => $user->picture,
            'created_at' => $user->createdAt,
            'gender' => $user->gender,
            'idFoursquare' => $user->idFoursquare,
            'idInstagram' => $user->idInstagram,
            'idTwitter' => $user->idTwitter,
            'idGooglePlus' => $user->idGooglePlus,
            'idFacebook' => $user->idFacebook,
            'tokenFoursquare' => $user->tokenFoursquare,
            'tokenInstagram' => $user->tokenInstagram,
            'tokenTwitter' => $user->tokenTwitter,
            'tokenGooglePlus' => $user->tokenGooglePlus,
            'tokenFacebook' => $user->tokenFacebook
        );
    }

    /**
     * Turns an array of data into a User object
     *
     * @param array $userArr
     * @param User $user
     * @return User
     */
    public function arrayToObject( $userArr, $user = null)
    {
        // create a User, unless one is given
        if (!$user) {
            $user = new User();

            $user->id = isset($userArr['id']) ? $userArr['id'] : null;
        }

        $username = isset($userArr['username']) ? $userArr['username'] : null;
        $name = isset($userArr['name']) ? $userArr['name'] : null;
        $password = isset($userArr['password']) ? $userArr['password'] : null;
        $email = isset($userArr['email']) ? $userArr['email'] : null;
        $picture = isset($userArr['picture']) ? $userArr['picture'] : null;
        $gender = isset($userArr['gender']) ? $userArr['gender'] : null;
        $createdAt = isset($userArr['created_at']) ? \DateTime::createFromFormat(self::DATE_FORMAT, $userArr['created_at']) : null;

        $idFoursquare = isset($userArr['idFoursquare']) ? $userArr['idFoursquare'] : null;
        $idInstagram = isset($userArr['idInstagram']) ? $userArr['idInstagram'] : null;
        $idTwitter = isset($userArr['idTwitter']) ? $userArr['idTwitter'] : null;
        $idGooglePlus = isset($userArr['idGooglePlus']) ? $userArr['idGooglePlus'] : null;
        $idFacebook = isset($userArr['idFacebook']) ? $userArr['idFacebook'] : null;

        $tokenFoursquare = isset($userArr['tokenFoursquare']) ? $userArr['tokenFoursquare'] : null;
        $tokenInstagram = isset($userArr['tokenInstagram']) ? $userArr['tokenInstagram'] : null;
        $tokenTwitter = isset($userArr['tokenTwitter']) ? $userArr['tokenTwitter'] : null;
        $tokenGooglePlus = isset($userArr['tokenGooglePlus']) ? $userArr['tokenGooglePlus'] : null;
        $tokenFacebook = isset($userArr['tokenFacebook']) ? $userArr['tokenFacebook'] : null;

        //data
        if ($username) {
            $user->username = $username;
        }

        if ($name) {
            $user->name = $name;
        }

        if ($password) {
            $user->password = $password;
        }

        if ($email) {
            $user->email = $email;
        }

        if ($picture) {
            $user->picture = $picture;
        }

        if ($gender) {
            $user->gender = $gender;
        }

        if ($createdAt) {
            $user->createdAt = $createdAt;
        }

        //ids
        if ($idFoursquare) {
            $user->idFoursquare = $idFoursquare;
        }

        if ($idInstagram) {
            $user->idInstagram = $idInstagram;
        }

        if ($idTwitter) {
            $user->idTwitter = $idTwitter;
        }

        if ($idGooglePlus) {
            $user->idGooglePlus = $idGooglePlus;
        }

        if ($idFacebook) {
            $user->idFacebook = $idFacebook;
        }

        //token
        if ($tokenFoursquare) {
            $user->tokenFoursquare = $tokenFoursquare;
        }

        if ($tokenInstagram) {
            $user->tokenInstagram = $tokenInstagram;
        }

        if ($tokenTwitter) {
            $user->tokenTwitter = $tokenTwitter;
        }

        if ($tokenGooglePlus) {
            $user->tokenGooglePlus = $tokenGooglePlus;
        }

        if ($tokenFacebook) {
            $user->tokenFacebook = $tokenFacebook;
        }

        return $user;
    }

    public function loadUserByUsername($username)
    {

        $user = $this->findOneByUsername($username);

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return $this->arrayToObject($user->toArray());
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Wingman\Entity\User';
    }


    private function encodePassword(User $user)
    {
        if ($user->plainPassword) {
            $user->password = $this->passwordEncoder->encodePassword($user->plainPassword, $user->getSalt());
        }
    }
}