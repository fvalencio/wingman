<?php

namespace Wingman\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Wingman\Entity\UserRepository")
 */
class User implements UserInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $password;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $plainPassword;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $email;

    /**
     * @ORM\Column(type="string", length=256)
     */
    public $picture;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $roles = array('ROLE_USER');

    /**
     * @ORM\Column(type="integer", length=11)
     */
    public $gender;

    /**
     * @ORM\Column(type="datetime", length=100)
     */
    public $createdAt;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $idFoursquare;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $idInstagram;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $idTwitter;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $idGooglePlus;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $idFacebook;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $tokenFoursquare;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $tokenInstagram;

    /**
     * @ORM\Column(type="string", length=128)
     */
    public $tokenTwitter;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $tokenGooglePlus;

    /**
     * @ORM\Column(type="string", length=128)
     */
    public $tokenFacebook;


    public function __construct()
    {
        $this->createdAt = new \Datetime();
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $idFacebook
     */
    public function setIdFacebook($idFacebook)
    {
        $this->idFacebook = $idFacebook;
    }

    /**
     * @return mixed
     */
    public function getIdFacebook()
    {
        return $this->idFacebook;
    }

    /**
     * @param mixed $idFoursquare
     */
    public function setIdFoursquare($idFoursquare)
    {
        $this->idFoursquare = $idFoursquare;
    }

    /**
     * @return mixed
     */
    public function getIdFoursquare()
    {
        return $this->idFoursquare;
    }

    /**
     * @param mixed $idGooglePlus
     */
    public function setIdGooglePlus($idGooglePlus)
    {
        $this->idGooglePlus = $idGooglePlus;
    }

    /**
     * @return mixed
     */
    public function getIdGooglePlus()
    {
        return $this->idGooglePlus;
    }

    /**
     * @param mixed $idInstagram
     */
        public function setIdInstagram($idInstagram)
    {
        $this->idInstagram = $idInstagram;
    }

    /**
     * @return mixed
     */
    public function getIdInstagram()
    {
        return $this->idInstagram;
    }

    /**
     * @param mixed $idTwitter
     */
    public function setIdTwitter($idTwitter)
    {
        $this->idTwitter = $idTwitter;
    }

    /**
     * @return mixed
     */
    public function getIdTwitter()
    {
        return $this->idTwitter;
    }

    /**
     * @param mixed $tokenFacebook
     */
    public function setTokenFacebook($tokenFacebook)
    {
        $this->tokenFacebook = $tokenFacebook;
    }

    /**
     * @return mixed
     */
    public function getTokenFacebook()
    {
        return $this->tokenFacebook;
    }

    /**
     * @param mixed $tokenFoursquare
     */
    public function setTokenFoursquare($tokenFoursquare)
    {
        $this->tokenFoursquare = $tokenFoursquare;
    }

    /**
     * @return mixed
     */
    public function getTokenFoursquare()
    {
        return $this->tokenFoursquare;
    }

    /**
     * @param mixed $tokenGooglePlus
     */
    public function setTokenGooglePlus($tokenGooglePlus)
    {
        $this->tokenGooglePlus = $tokenGooglePlus;
    }

    /**
     * @return mixed
     */
    public function getTokenGooglePlus()
    {
        return $this->tokenGooglePlus;
    }

    /**
     * @param mixed $tokenInstagram
     */
    public function setTokenInstagram($tokenInstagram)
    {
        $this->tokenInstagram = $tokenInstagram;
    }

    /**
     * @return mixed
     */
    public function getTokenInstagram()
    {
        return $this->tokenInstagram;
    }

    /**
     * @param mixed $tokenTwitter
     */
    public function setTokenTwitter($tokenTwitter)
    {
        $this->tokenTwitter = $tokenTwitter;
    }

    /**
     * @return mixed
     */
    public function getTokenTwitter()
    {
        return $this->tokenTwitter;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'username' => $this->getUsername(),
            'salt' => $this->getSalt(),
            'email' => $this->getEmail(),
            'picture' => $this->getPicture(),
            'password'=>$this->getPassword(),
            'gender' => $this->getGender(),
            'idFoursquare' => $this->getIdFoursquare(),
            'idInstagram' => $this->getIdInstagram(),
            'idTwitter' => $this->getIdTwitter(),
            'idGooglePlus' => $this->getIdGooglePlus(),
            'idFacebook' => $this->getIdFacebook(),
            'tokenFoursquare' => $this->getTokenFoursquare(),
            'tokenInstagram' => $this->getTokenInstagram(),
            'tokenTwitter' => $this->getTokenTwitter(),
            'tokenGooglePlus' => $this->getTokenGooglePlus(),
            'tokenFacebook' => $this->getTokenFacebook()
        );
    }

}
